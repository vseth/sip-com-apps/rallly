#!/bin/sh

export DATABASE_URL="postgres://$SIP_POSTGRES_RALLLY_USER:$SIP_POSTGRES_RALLLY_PW@$SIP_POSTGRES_RALLLY_SERVER:$SIP_POSTGRES_RALLLY_PORT/$SIP_POSTGRES_RALLLY_NAME"

export OIDC_NAME="SwitchAAI"
export OIDC_DISCOVERY_URL=$SIP_AUTH_OIDC_DISCOVERY_URL
export OIDC_CLIENT_ID=$SIP_AUTH_RALLLY_CLIENT_ID
export OIDC_CLIENT_SECRET=$SIP_AUTH_RALLLY_CLIENT_SECRET

./docker-start.sh
