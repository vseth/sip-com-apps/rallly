FROM lukevella/rallly:latest

COPY entrypoint.sh .

CMD ["./entrypoint.sh"]
